// unordered set example
// ~ unique elements only (NOT sorted)

#include <iostream>
#include <string>
#include <unordered_set>

int main() {
    std::unordered_set<std::string> set{ "Ala", "Ma", "Kota", "A", "Kot", "Ma", "ALE" };

    for (const auto el : set) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    set.insert("Ala"); // cannot be added (already exists)

    for (const auto el : set) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
