// lambda call as a function

#include <iostream>

int main() {
    // lambda definition
    [] {
        std::cout << "Hello\n";
    };

    // lambda definition & call
    [] {
        std::cout << "Hello World!\n";
    }();

    // empty lambda call
    [] {}();
    // or..
    []() {}();

    int n = 9;
    // pass an argument
    [](int n) {
        std::cout << "Arg: " << n << '\n';
    }(n);

    // or capture by value.. (const by default)
    [n]() {
        std::cout << "Value: " << n << '\n';  // n is const
    }();
    // or capture by value.. (NOT const)
    [n]() mutable {
        std::cout << "Mutable: " <<++n << '\n';  // n incremented in lambda only
    }();
    // or capture by reference
    [&n]() {
        std::cout << "Ref: " << (n += 9) << '\n';  // n incremented OUTSIDE lambda
    }();
    // as a proof
    [n]() {
        std::cout << "Proof: " << n << '\n';  // n incremented INDEED
    }();

    // pass by ref vs value
    int a{ 4 }, b{ 8 }, c{ 2 };
    [=, &b, &c]() {
        std::cout << a << " : " << ++b << " : " << ++c << '\n'; // a by value
    }();

    return 0;
}