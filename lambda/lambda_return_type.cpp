// lambda return type

#include <iostream>

int main() {
    // deducted return type (here: int)
    [i{ 0 }](const int el) { return el + i; }; // capture list initialization

    // requested return type double
    [i{ 3.14 }](const auto el) -> double { return el + i; };

    return 0;
}