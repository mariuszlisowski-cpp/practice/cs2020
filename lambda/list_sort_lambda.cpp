// list sort lambda

// #include <algorithm>
#include <deque>
#include <forward_list>
#include <iostream>
#include <string>

std::deque<std::string> lengthSort(std::forward_list<std::string>& words) {
    // sorting with build-in predicate
    words.sort();
    // sorting with custom predicate
    words.sort([](const std::string& lhs, const std::string& rhs) {
        if (lhs.size() != rhs.size()) {
            return lhs.size() < rhs.size();  // sort by size
        }
        return lhs < rhs;  // sort lexicographically
    });

    // returning deque via instanitation
    return std::deque<std::string>{ words.begin(), words.end() };
}

template <typename Container>
void printCollection(const Container& collection) {
    for (const auto& element : collection) {
        std::cout << element << '\n';
    }
}

int main() {
    std::forward_list<std::string> words{
        "Somebody", "once", "told", "me", "the", "world",
        "is", "gonna", "roll", "me"
    };

    lengthSort(words);
    printCollection(words);

    return 0;
}