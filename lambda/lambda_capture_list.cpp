// lambda capture list
// ~ inside []

#include <iostream>

int main() {
    int val1{ 10 };
    int val2{ 20 };
    int val3{ 40 };

    // number captured in [] by value (always const)
    auto addA = [val1](int num) { return num + val1; };

    // all captured by value [=] (here: val2)
    auto addB = [=](int num) { return num + val2; };  // SAFE

    // all captured by reference [&] (here: val3)
    auto addC = [&](int num) { return num + val3; };  // UNSAFE

    // argument (int num)
    std::cout << addA(2) << '\n';  // argument (here: 2)
    std::cout << addB(4) << '\n';  // argument (here: 4)
    std::cout << addC(9) << '\n';  // argument (here: 9)

    return 0;
}
