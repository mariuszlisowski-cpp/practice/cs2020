// static method & variables access

#include <iostream>
#include <string>

// first method
class ObjectA {
public:
    // static std::string name_;
    static std::string getName() { return "ObjectA"; }
};

// second method
class ObjectB {
public:

    static std::string name_;
};

// static definition (in .cpp file)
std::string ObjectB::name_{ "ObjectB" };

class Regular {
public:
    std::string getName() { return name_; }

private:
    const std::string name_{ "Regular object" };
};

int main() {
    // static method & variables access
    std::cout << ObjectA::getName() << '\n';
    std::cout << ObjectB::name_ << '\n';

    // regular access
    Regular obj;
    std::cout << obj.getName() << '\n';

    return 0;
}
