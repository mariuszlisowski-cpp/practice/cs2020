// constructors inheritance

struct A {
    explicit A(int);
    int a;
};

struct B : A {
    using A::A;   // implicit declaration of B::B(int)
    B(int, int);  // overloaded inherited Base ctor
};

int main() {
    return 0;
}
