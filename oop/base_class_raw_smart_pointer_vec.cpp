// base class raw & smart pointer vector
// #polymorphismp #virtual #override

#include <iostream>
#include <memory>
#include <vector>

// base class
class Soundable {
public:
    virtual void makeSound() = 0;
};

// derived class
class Goose : public Soundable {
public:
    void makeSound() override { std::cout << "Honk!\n"; }
};

// derived class
class Hen : public Soundable {
public:
    void makeSound() override { std::cout << "Cluck!\n"; }
};

// derived class
class Duck : public Soundable {
public:
    void makeSound() override { std::cout << "Quack!\n"; }
};

template <class T>
void showVec(T);

int main() {
    // raw pointer version
    std::vector<Soundable*> birds;
    Goose goose;
    Hen hen;
    Duck duck;
    birds.push_back(&goose);
    birds.push_back(&hen);
    birds.push_back(&duck);
    showVec(birds);

    // smart pointer version
    std::vector<std::shared_ptr<Soundable>> birds_;

    birds_.push_back(std::make_shared<Goose>());
    birds_.push_back(std::make_shared<Hen>());
    birds_.push_back(std::make_shared<Duck>());
    showVec(birds_);

    return 0;
}

template <class T>
void showVec(T vec) {
    for (const auto& el : vec) {
        el->makeSound();
    }
}
