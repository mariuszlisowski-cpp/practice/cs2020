// hacking private class value

#include <iostream>

using namespace std;

class A {
private:
    const int& value;

public:
    A(int &a) : value(a) {}
    
    int getValue() { return value; }
    int* getValuePtr() { return (int*)&value; }
};

int main(int argc, char *argv[]){
    int i = 5;
    int j = 10;

    A a(i);

    cout << a.getValue() << endl;
    
    *(int**)&a = &j; // the key step that changes A's member reference

    cout << a.getValue() << endl;

    return  0;
}
