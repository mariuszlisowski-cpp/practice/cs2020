// base class raw & smart pointer

#include <iostream>
#include <memory>
#include <vector>

// base class
class Doppler {
public:
    virtual ~Doppler() { std::cout << "- BASE - D'tor caller\n"; };
    virtual void sayHello() { std::cout << "I'm Doppler!"; }
};

class Dwarf : public Doppler {
public:
    ~Dwarf() override { std::cout << "DWARF D'tor caller\n"; };
    void sayHello() override { std::cout << "I'm Dwarf!\n"; }
};

class Elf : public Doppler {
public:
    ~Elf() override { std::cout << "ELF D'tor caller\n"; };
    void sayHello() override { std::cout << "I'm Elf!\n"; }
};

class Human : public Doppler {
public:
    ~Human() override { std::cout << "HUMAN D'tor caller\n"; };
    void sayHello() override { std::cout << "I'm Human!\n"; }
};

int main() {
    // smart pointer version
    std::shared_ptr<Doppler> creature1 = std::make_shared<Dwarf>();
    std::shared_ptr<Doppler> creature2 = std::make_shared<Elf>();
    std::shared_ptr<Doppler> creature3 = std::make_shared<Human>();

    creature1->sayHello();
    creature2->sayHello();
    creature3->sayHello();
    
    // raw pointer version
    Doppler* being1 = new Dwarf;
    Doppler* being2 = new Elf;
    Doppler* being3 = new Human;

    being1->sayHello();
    being2->sayHello();
    being3->sayHello();

    delete being1;
    delete being2;
    delete being3;

    return 0;
}

