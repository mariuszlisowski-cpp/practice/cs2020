// polymorphism example

#include <iostream>

class Base {
public:
    virtual ~Base() {}

    virtual void nameMe() = 0;
};

class DerivedA : public Base {
public:
    void nameMe() {
        std::cout << "I'm a derived class A" << std::endl;
    }
};

class DerivedB : public Base {
public:
    void nameMe() {
        std::cout << "I'm a derived class B" << std::endl;
    }
};

void showUp(Base* ptr) {
    ptr->nameMe();
}

int main() {
    Base* poli{};

    // heap
    poli = new DerivedA;
    showUp(poli);
    delete poli;

    poli = new DerivedB;
    showUp(poli);
    delete poli;

    // stack
    DerivedA objA;
    poli = &objA;
    showUp(poli);

    DerivedB objB;
    poli = &objB;
    showUp(poli);

    return 0;
}