// class passed to class usage 

#include <iostream>
#include <string>

class Hand {
public:
    Hand(int hour, int minute) : 
        minute_(minute),
        hour_(hour)
    {}

    int getMinute() const {
        return minute_;
    }
    int getHour() const {
        return hour_;
    }

private:
    int minute_;
    int hour_;
};

class Clock {
public:
    Clock(Hand* hand) : hand_(hand) {}

    std::string getTime() const {
        return std::to_string(hand_->getHour()) + ":" +
               std::to_string(hand_->getMinute());
    }

private:
    Hand* hand_;
};

int main() {
    Hand hand(17, 59);
    
    Clock clock(&hand);
    std::string time = clock.getTime();

    std::cout << time << std::endl;

    return 0;
}
