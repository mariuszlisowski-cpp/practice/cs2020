// template specialization many types 

#include <cmath>
#include <iostream>

// no specialization
template <typename T1, typename T2>
struct X {
    T1 x;
    T2 y;
    void getX() {}
};
// specialization of one type
template <typename T2>
struct X<char, T2> {
    char ch;
    T2 value;
};
// specialization of all types
template <>
struct X<double, double> {
    int radius;
    double circumference;
};

int main() {
    X<int, int> point;
    point.x = 2;
    point.y = 3;

    X<char, int> ascii;
    ascii.ch = 'a';
    ascii.value = 65;

    X<double, double> circle;
    circle.radius = 1;
    circle.circumference = 2 * M_PI * circle.radius;

    return 0;
}
