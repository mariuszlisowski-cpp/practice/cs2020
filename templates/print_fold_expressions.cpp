// print fold expressions 

#include <iostream>

using namespace std;

template <typename... Args>
void printFolded(Args&&... args) {
    (std::cout << ... << args) << std::endl; // without spaces
    // or:
    ((std::cout << args << ' '), ...) << std::endl; // with
}

template <typename... Args>
bool areEven(Args... args) {
    printFolded(args...);
    return ((args % 2 == 0) && ...);
}

int main() {
    printFolded("ala", "ma", 99, "kotow", '!');
    
    auto resultA = areEven(0, 2, 4, 6, 8); // all even
    auto resultB = areEven(1, 2, 4, 6, 8); // 1 is odd

    std::cout << std::boolalpha << resultA << std::endl; // true
    std::cout << std::boolalpha << resultB << std::endl; // false

    return 0;
}
