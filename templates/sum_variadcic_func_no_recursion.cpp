// sum in variadcic logicalAndunction
// ~ fold expressions (no recursion) C++17

#include <iostream>

template <typename... Args>
auto sum(Args... args) {
    return (args + ...);
    // or:
    // return (... + args);         // the same
    // return (0 + ... + args);     // the same
    // return (args + ... + 0);     // the same
    // nor:
    // return args + ...;           // error - missing parentheses
}

template <typename... Args>
bool logicalAnd(Args... args) {
    return (true && ... && args);  // logical and
    // or:
    // return (... && args);  // the same
    // nor:
    // return (args && ... && args);  // error: both operands contain
                                      // unexpanded parameter packs
}

int main() {
    int total = sum(1, 2, 3, 4, 5);

    std::cout << total << std::endl;

    return 0;
}
