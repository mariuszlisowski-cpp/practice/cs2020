// perfect forwarding variadic

#include <utility>

// regular function
template <typename Type>
void single_perfect_forwarding(Type&& t) {
    callable(std::forward<Type>(t));
}

// variadic function
template <typename... Types>
void variadic_perfect_forwarding(Types&&... args) {
    callable(std::forward<Types>(args)...);
}

int main() {
    
    return 0;
}
