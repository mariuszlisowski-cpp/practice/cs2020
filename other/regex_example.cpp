
#include <string>
#include <iostream>
#include <regex>

std::string checkPass(std::string pass) {
    std::regex upperCaseChar("[A-Z]");
    std::regex lowerCaseChar("[a-z]");
    std::regex specialChar("[$&+,:;=?@#|'<>.^*()%!]");
    std::regex oneNumber("[0-9]");

    if (pass.size() < 6)
        return "Too short";
    else if(!std::regex_search(pass, upperCaseChar))
        return "No uppercase character";
    else if(!std::regex_search(pass, lowerCaseChar))
        return "No lowercase character";
    else if(!std::regex_search(pass, specialChar))
        return "No special character";
    else if(!std::regex_search(pass, oneNumber))
        return "No number";
    else
        return "Password OK";
}

int main() {
    std::string s {"Abc!123"};

    std::cout << checkPass(s) << '\n';

    return 0;
}