// functor stl containers

#include <algorithm>
#include <deque>
#include <iostream>
#include <set>

// method inside can be as a functor
class Lsb_less {
public:
    // type name (must be const)
    bool operator()(int x, int y) const {
        return (x % 10) < (y % 10);
    }
};

// is not a type name
// cannot be used as a functor
bool lsb_less(int x, int y) {
    return (x % 10) < (y % 10);
}

int main() {
    // associative container (already sorted)
    std::set<int> myset1 { 3, 1, 25, 7, 12 }; // { 1, 3, 7, 12, 25 }
    // sorted using 'less' functor by default
    std::set<int, std::less<int>> myset2 { 3, 1, 25, 7, 12 };

    // custom sort
    std::set<int, Lsb_less> myset3 { 3, 1, 25, 7, 12 };

    // display
    for (const auto& el : myset3) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
