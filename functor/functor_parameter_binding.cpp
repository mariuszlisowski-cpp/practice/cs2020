// functor parameter binding
// ~ binds a functor argument
// #placeholders

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

// two argument funtion (used as a functor)
void addVal(int arg1, int arg2) {
    std::cout << arg1 + arg2 << ' ';
}

int main() {
    std::set<int> myset{ 1, 2, 3, 4, 5 };  // input
    std::vector<int> v;                    // output

    // buil-in functor example (not used)
    int x = std::multiplies<int>()(3, 4);

    // first parameter of functor is substituted with myset's element
    // std::mulitplies<int>()(1, 10);
    // std::mulitplies<int>()(2, 10);
    // std::mulitplies<int>()(3, 10);
    std::transform(myset.begin(), myset.end(),  // input
                   std::back_inserter(v),       // output
                   std::bind(std::multiplies<int>(), std::placeholders::_1, 10));

    // display
    for (const auto& el : v) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    // custom functor binding
    for_each(v.begin(), v.end(), std::bind(addVal, std::placeholders::_1, 5));

    return 0;
}
