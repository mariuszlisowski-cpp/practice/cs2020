// functor function convert
// ~ convert a regular function to a functor
// #transform #placeholders

#include <algorithm>
#include <cmath>
#include <deque>
#include <iostream>
#include <set>

// regular function to be cast
double power(double x, double y) {
    return std::pow(x, y);
}

int main() {
    std::set<int> myset{ 3, 1, 25, 7, 12 };  // input
    std::deque<int> d;                       // output

    // casting to a functor
    auto functor = std::function<double(double, double)>(power);

    // power every element of set
    int exponent = 2;
    std::transform(myset.begin(), myset.end(),  // source
                   std::back_inserter(d),       // destination
                   bind(functor, std::placeholders::_1, exponent));

    // display
    for (const auto& el : d) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
