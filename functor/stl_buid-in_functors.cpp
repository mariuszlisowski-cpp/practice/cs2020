// stl buid-in functors

#include <algorithm>
#include <iostream>
#include <vector>


int main() {
    // std::less;
    // std::greater;
    // std::greater_equal;
    // std::less_equal;
    // std::not_equal_to;
    // std::logical_and;
    // std::logical_not;
    // std::logical_or;
    // std::multiplies;
    // std::minus;
    // std::plus;
    // std::divides;
    // std::modulus;
    // std::negate;

    int x = std::multiplies<int>()(3, 4);
    
    if (std::not_equal_to<int>()(x ,10)) // (x != 10)
        std::cout << x << '\n';

    return 0;
}
