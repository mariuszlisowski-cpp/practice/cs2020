// functor example

#include <iostream>

struct Functor {
    void operator()() {
        std::cout << "I'm a functor in a structure\n";
    }
};

class Example {
public:
    int value;
    Example(int i = 0):value(i) {}
    void operator()(std::string str) {
        std::cout << str << value << '\n';
    }
};

void function() {
    std::cout << "I'm a functor/function\n";
}

int main() {
    Functor func;
    // structure as a functor
    func();

    // temporary object functor
    Functor{}();  // without a name

    // class wit a functor
    Example ex;
    ex("I'm a functor in a class");
    
    // calling construcor & functor
    Example(5)("Passed: ");

    // regular function as a functor
    function();

    return 0;
}
