// functor predicate example
// ~ a functor that:
// - returns a boolean
// - does not modify data

#include <algorithm>
#include <deque>
#include <iostream>
#include <set>

class NeedCopy {
public:
    // functor method
    // precidate: boolean & const
    // (argument passed by value)
    bool operator()(int x) {
        return (x % 10) == 0;
    }
};

int main() {
    // sorted container
    std::set<int> myset { 3, 10, 25, 70, 12 };
    std::deque<int> queue1;
    std::deque<int> queue2;

    // using a predicate form class
    std::copy_if(myset.begin(), myset.end(),
                 std::back_inserter(queue1), NeedCopy());

    // using as lambda
    std::copy_if(myset.begin(), myset.end(),
                 std::back_inserter(queue2),
                 [](int x) { return (x % 10) == 0; });

    // display
    for (const auto& el : queue1) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
