// dangling pointer

#include <iostream>

int doNothing() {
/* G */ int b = 50;
        int c = 100;
/* H */ return b;
}

int* getObject() {
/* C */ int a = 10;
        int* ptr = &a;
/* D */ return ptr; // a variable goes out of scope
}

int main() {
    /* A */ int number = 5;
    /* B */ int* pointer = getObject(); // dangling pointer
    /* E */ *pointer = 20; // undefinied behaviour
    /* F */ number = doNothing();
    /* I */ std::cout << *pointer << '\n';
}