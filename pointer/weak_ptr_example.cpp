// weak pointer example
// #expired() #lock()

#include <iostream>
#include <memory>

using namespace std;


int main() {
    weak_ptr<int> weak;
    
    cout << "expired: " << weak.expired() << "; address: " << weak.lock() << '\n';
    // another scope
    {
        shared_ptr<int> strong(new int);
        weak = strong;
        cout << "expired: " << weak.expired() << "; address: " << weak.lock() << '\n';
    }
    // scope exitted
    cout << "expired: " << weak.expired() << "; address: " << weak.lock() << '\n';
}