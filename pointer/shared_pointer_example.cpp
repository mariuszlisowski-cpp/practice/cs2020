// shared pointers example

#include <iostream>
#include <memory>

int main() {

    int a = 5;

    // declaration & initialization
    auto ptr1 = std::make_shared<int>(a); // preferred
    auto ptr2 = std::shared_ptr<int>(new int{10});

    std::cout << *ptr1 << std::endl;
    std::cout << *ptr2 << std::endl;

    return 0;
}
