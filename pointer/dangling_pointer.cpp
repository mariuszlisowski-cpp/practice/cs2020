// dangling pointer

#include <iostream>
#include <memory>

void foo(int* num) {
    if (num) {
        (*num)++;
    }
    else 
        std::cout << "Empty pointer passed";
}

int main() {
    auto ptr = std::make_shared<int>(5);
    int* raw = ptr.get();

    ptr.reset(); // delete object, deallocate memory

    foo(ptr.get()); // not a problem, nullptr is passed
    foo(raw); // problem, dangling pointer is passed

    return 0;
}