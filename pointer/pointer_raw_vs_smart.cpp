// raw vs smart pointer

#include <iostream>
#include <memory>

struct Msg {
    int getValue() {
        return 77;
    }
};

Msg* createMsgRaw() {
    return new Msg;
}

std::unique_ptr<Msg> createMsgSmart() {
    return std::make_unique<Msg>();
}

int main() {
    // raw
    auto msgRaw = createMsgRaw(); // auto = Msg*
    std::cout << msgRaw->getValue() << std::endl;
    delete msgRaw;

    // smart
    auto msgSmart = createMsgSmart(); // auto = std::unique_ptr
    std::cout << msgSmart->getValue() << std::endl; // same usage
    // no delete needed

    return 0;
}