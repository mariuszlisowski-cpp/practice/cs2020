// cyclic dependencies 
// ~ memory leak here

#include <memory>
struct Node {
    std::shared_ptr<Node> child;
    std::shared_ptr<Node> parent;
};
int main() {
     auto sharedA = std::shared_ptr<Node>(new Node);
    auto sharedB = std::shared_ptr<Node>(new Node);

    sharedA->child = sharedB;
    sharedB->parent = sharedA;
}
