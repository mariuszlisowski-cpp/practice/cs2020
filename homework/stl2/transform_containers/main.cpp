#include "transform.hpp"

#include <iostream>

int main() {
    std::list<std::string> list{
        "Ala", "Kot", "Ma", "Rysia", "Ala",
        "Sierotka", "Kot", "Ma", "Ala"};

    std::deque<int> deque{
        1, 2, 3, 4, 5, 3, 1, 2, 3, 4,
        5, 2, 3, 1, 1, 2, 3, 2, 1, 4};

    auto  transformed = removeDuplicateAndTranformToMap(list, deque);

    for (const auto& pair : transformed) {
        std::cout << pair.first << " : " << pair.second << '\n';
    }

    return 0;
}
