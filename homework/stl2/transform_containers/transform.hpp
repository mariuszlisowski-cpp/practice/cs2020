#pragma once

#include <deque>
#include <list>
#include <map>
#include <string>

std::map<int, std::string> removeDuplicateAndTranformToMap(
    const std::list<std::string>&,
    const std::deque<int>&);
