#pragma once

#include <string>

bool is_palindrome(const std::string& str);
void test(const std::string& s);
