#pragma once

#include <array>
#include <cstdint>
#include <vector>

constexpr size_t width = 32;
constexpr size_t height = 32;
constexpr uint8_t min_print = 33;

using CompressedBitmap = std::vector<std::pair<uint8_t, uint8_t>>;
using Bitmap = std::array<std::array<uint8_t, width>, height>;

CompressedBitmap compressGrayscale(const Bitmap& bitmap);

Bitmap decompressGrayscale(const CompressedBitmap& compressed);

void printMap(const Bitmap& bitmap);
