// fibonacci iterative & recursive (optimized)

#include <iostream>

using namespace std;

constexpr long long fibRecursive(int n) {
    if (n <= 1)
        return n;
    return fibRecursive(n - 1) + fibRecursive(n - 2);
}

long long fibIterative(int n) {
    if (n <= 1)
        return n;
    long long last = 1;
    long long nextToLast = 1;
    long long result = 1;
    for (int i = 2; i < n; ++i) {
        result = last + nextToLast;
        nextToLast = last;
        last = result;
    }
    return result;
}

int main() {
    // for 'constexpr' result must be saved here 
    long long resultR = fibRecursive(45);
    long long resultI = fibIterative(45);

    // 'constexpr' thus cannot use function inside 'stream'
    cout << "Recursive: " << resultR << endl;
    cout << "Iterative: " << resultI << endl;

    return 0;
}
