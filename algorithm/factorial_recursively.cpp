// factorial recursively
// ~ under linux run with preceding 'time ./factorial'
// ~ to measure execution time with and withour constexpr
// 
// fac(n) = n * fac(n-1)
// fac(5) = 5 * fac( 4 ) = 5 * 24 = 120
// fac(4) = 4 * fac( 3 ) = 4 * 6 = 24
// fac(3) = 3 * fac( 2 ) = 3 * 2 = 6
// fac(2) = 2 * fac( 1 ) = 2 * 1 = 2
// fac(1) = 1 * fac( 0 ) = 1
// fac(0) = 1

#include <iostream>

// regular (short)
unsigned long long facR(int n) {
    return n == 0 ? 1 : n * facR(n - 1);
}

// constexpr (short)
constexpr unsigned long long facC(int n) {
    return n == 0 ? 1 : n * facC(n - 1);
}

int main() {
    // std::cout << "Calculating factorial..." << std::endl;
    // unsigned long long factorialR = facR(60); // quick
    // std::cout << factorialR << std::endl;

    std::cout << "Ready factorial..." << std::endl;
    constexpr unsigned long long factorialC = facC(22); // quick
    std::cout << factorialC << std::endl;

    return 0;
}
