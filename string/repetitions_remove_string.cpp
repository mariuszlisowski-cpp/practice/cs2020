// remove repetitions in a string

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

template <class Container>
void printContainer(Container con) {
    for (const auto& el : con) {
        std::cout << "\"" << el << "\"" << '\n';
    }
    // std::cout << '\n';
}

auto removeDuplicates(std::string& str) {
    std::istringstream iss(str);

    std::vector<std::string> words(
        std::istream_iterator<std::string>{ iss },
        std::istream_iterator<std::string>());

    // verbose
    // printContainer(words);

    // move all consecutive equal words to the end
    auto itr = std::unique(words.begin(),
                           words.end());
    // remove unwanted words
    words.erase(itr,
                words.end());
    
    // verbose

    std::transform(words.begin(),
                   words.end(),
                   words.begin(),
                   [](std::string s) {
                       s.push_back(' ');
                       return s;
                   });
    printContainer(words);

    std::string resultString;
    
    auto result = accumulate(begin(words),
                             end(words),
                             resultString);
    
    std::cout << result << '\n';

    return result;
}


int main() {
    std::string text = "Ala ma ma kota, a kot ma ma Ale Ale.";

    std::cout << "Original text:\t\t" << text << '\n';

    removeDuplicates(text);

    // std::cout << "Duplicates removed:\t" << removeDuplicates(text) << '\n';

    return 0;
}
