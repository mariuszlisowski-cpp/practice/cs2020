// string accumulate transform
// #transform #accumulate

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

template <class Container>
void printContainer(Container con) {
    for (const auto& el : con) {
        std::cout << el << '\n';
    }
}

int main() {
    std::string str{ "Split string example word by word" };
    std::istringstream iss(str);
    // splitting string to words
    std::vector<std::string> words(
        std::istream_iterator<std::string>{ iss },
        std::istream_iterator<std::string>());
    // verbose
    std::cout << str << '\n';
    printContainer(words);
    // adding space to each word
    std::transform(words.begin(),
                   words.end(),
                   words.begin(),
                   [](std::string s) {
                       s.push_back(' ');
                       return s;
                   });
    // joining into a string
    std::string result = std::accumulate(words.begin(),
                                         words.end(),
                                         std::string());
    // verbose
    std::cout << result << '\n';

    return 0;
}
