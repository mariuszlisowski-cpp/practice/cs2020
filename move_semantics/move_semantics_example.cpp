// move semantics example

#include <iostream>

template <typename T>
void foo(T && a) {
    std::cout << "OK\n";
}

int main() {
    int a = 5;
    
    foo(4);
    foo(a);
    foo(std::move(a));

    return 0;
}