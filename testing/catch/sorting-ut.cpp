// unit tests
// verbose all tests: ./sorting-ut -s

#include "catch.hpp"

#include <algorithm>
#include <string>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
TEST_CASE("Sorting a vector of permuted 1, 2, 3 shout give 1, 2, 3", "[sort]") {
    const std::vector<int> expected{ 1, 2, 3 };

    SECTION("sorting 3, 2, 1") {
        std::vector<int> v{ 3, 2, 1 };
        std::sort(std::begin(v), std::end(v));

        REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
    }

    SECTION("sorting 1, 2, 3") {
        std::vector<int> v{ 2, 1, 3 };
        std::sort(std::begin(v), std::end(v));

        REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
    }
}
////////////////////////////////////////////////////////////////////////////////
SCENARIO("A container should be sorted", "[sort]") {

    GIVEN("a vector with some items") {
        std::vector<int> v{ 2, 1, 3 };

        WHEN("is sorted ascending") {
            std::sort(std::begin(v), std::end(v));

            THEN("is in ascending order") {
                const std::vector<int> expected{ 1, 2, 3 };
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
        WHEN("is sorted descending") {
            std::sort(std::rbegin(v), std::rend(v));
            
            THEN("is in descending order") {
                const std::vector<int> expected{ 3, 2, 1 };
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
    }

    GIVEN("a vector with zero or one item") {
        auto v = GENERATE(std::vector<int>{},
                          std::vector<int>{ 1 });
        std::vector<int> expected;
        
        WHEN("is sorted") {
            std::sort(std::begin(v), std::end(v));

            THEN("vector does not change") {
                if (!v.empty()) {
                    expected.push_back(1);
                }
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
    }
    
    GIVEN("a string") {
        std::string text{ "text" };
        std::string expected{ "ettx" };

        WHEN("is sorted") {
            std::sort(std::begin(text), std::end(text));

            THEN("string should be sorted lexicographically") {
                REQUIRE_THAT(text, Catch::Matchers::Equals(expected));
            }
        }
    }
}


SCENARIO("A container should be sorted with a comparator", "[sort][comparator]") {

    GIVEN("a vector with some items") {
        std::vector<int> v{ 3, 2, 1, 4 };

        WHEN("sorting descending") {
            std::sort(std::begin(v),
                      std::end(v),
                      std::greater<int>());

            THEN("is sorted descending") {
                const std::vector<int> expected{ 4, 3, 2, 1 };
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
        WHEN("sorting ascending") {
            std::sort(std::begin(v),
                      std::end(v),
                      std::less<int>());

            THEN("is sorted ascending") {
                const std::vector<int> expected{ 1, 2, 3, 4 };
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
    }
}    
////////////////////////////////////////////////////////////////////////////////
SCENARIO("Two vectors should be concatenated", "[merge]") {
    GIVEN("the first vector with some items") {
        std::vector<int> v{ 1, 2, 3 };

        AND_GIVEN("the second vector with some items") {
            std::vector<int> w{ 4, 5, 6 };
            
            WHEN("both vectors are merged") {
                std::vector<int> expected{ 1, 2, 3, 4, 5, 6 };
                std::vector<int> concatenated;
                concatenated.reserve(v.size() + w.size());
                
                std::merge(std::begin(v), std::end(v),
                           std::begin(w), std::end(w),
                           std::back_inserter(concatenated));

                THEN("the vectors are concatenated") {
                    REQUIRE_THAT(concatenated, Catch::Matchers::Equals(expected));
                }
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
SCENARIO("A container should be sorted using generator", "[sort][generate]") {

    GIVEN("a vector with some items") {
        auto v = GENERATE(std::vector<int>{3, 2, 1},
                          std::vector<int>{3, 1, 2},
                          std::vector<int>{2, 1, 3},
                          std::vector<int>{2, 3, 1},
                          std::vector<int>{1, 2, 3},
                          std::vector<int>{1, 3, 2});
        const std::vector<int> expected{ 1, 2, 3 };

        WHEN("sorting ascending") {
            std::sort(std::begin(v), std::end(v));

            THEN("is sorted ascending") {
                REQUIRE_THAT(v, Catch::Matchers::Equals(expected));
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
SCENARIO("A container should be sorted using a pair", "[sort][pair]") {

    GIVEN("a vector with some items") {
        using Pair = std::pair<std::vector<int>, std::vector<int>>;
        auto pair = GENERATE(Pair{{},{}},
                             Pair {{1}, {1}});
        Pair expected;
        
        WHEN("sorting ascending") {
            std::sort(std::begin(pair.first), std::end(pair.first));
            std::sort(std::begin(pair.second), std::end(pair.second));

            THEN("is sorted ascending") {
                if (!pair.first.empty()) {
                    expected.first.push_back(1);
                }
                if (!pair.second.empty()) {
                    expected.second.push_back(1);
                }
                REQUIRE_THAT(pair.first, Catch::Matchers::Equals(expected.first));
                REQUIRE_THAT(pair.second, Catch::Matchers::Equals(expected.second));
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
