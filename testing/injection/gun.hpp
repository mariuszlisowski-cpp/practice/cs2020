#pragma once

#include <cstdio>

#include "weapon.hpp"

constexpr size_t INITIAL_CAPACITY = 50;

class Gun : public Weapon {
public:
    Gun();

    void shoot() override;
    void reload() override;

private:
    static const size_t capacity_ = INITIAL_CAPACITY;
    size_t bullets_;
};
