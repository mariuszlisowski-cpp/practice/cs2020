#include <cmath>

double squareRoot(const double input) {
    // if negarive input NaN exception will occur
    double result = std::sqrt(input);
    // NaN (Not a Number) check (for negative input)
    if (result != result) {
        return -1.0;
    } else {
        return sqrt(input);
    }
}
