#include "gun.hpp"

Gun::Gun() : bullets_(INITIAL_CAPACITY) {}

void Gun::shoot() {
    --bullets_;
}
void Gun::reload() {
    bullets_ = INITIAL_CAPACITY;
}
