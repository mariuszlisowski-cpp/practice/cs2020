// injected test weapon

#include "testWeapon.hpp"

void TestWeapon::shoot() {
    hasShot = true;
    hasReload = false;
}
void TestWeapon::reload() {
    hasShot = false;
    hasReload = true;
}
