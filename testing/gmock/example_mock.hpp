#pragma once

#include <gmock/gmock.h>

class Example;

class MockExample : public Example {
public:
    MOCK_CONST_METHOD0(GetSize,
                       int());
    MOCK_METHOD1(Describe,
                 std::string(const char* name));
    MOCK_METHOD1(Describe,
                 std::string(int type));
    MOCK_METHOD1(Process,
                 bool(int count));
};
