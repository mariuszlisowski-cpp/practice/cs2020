cmake_minimum_required(VERSION 2.8.2)

set(GMOCK_DIR /usr/src/googletest/googlemock)
add_subdirectory(${GMOCK_DIR} gmock)

project(Example)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

add_executable(${PROJECT_NAME}-ut test.cpp)
target_link_libraries(${PROJECT_NAME}-ut gmock_main)

enable_testing()
add_test(NAME ExampleTest COMMAND ${PROJECT_NAME}-ut)
