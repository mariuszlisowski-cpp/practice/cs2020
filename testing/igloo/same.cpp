#include <igloo/igloo_alt.h>

#include <algorithm>
#include <vector>

#include <iostream>

class Same {
public :
    static bool comp(std::vector<int>& a, std::vector<int>& b) {
        if (a.empty() && b.empty()) {
            return true;
        }
        else if (a.empty() || b.empty() || (a.size() != b.size())) {
            return false;
        }
        
        return a.size() == b.size();
    }
};

int main(int argc, char *argv[]) {
  return igloo::TestRunner::RunAllTests(argc, argv);
}
