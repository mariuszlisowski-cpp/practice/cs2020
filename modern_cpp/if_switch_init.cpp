// if switch init
// -std=c++17

#include <iostream>

bool isSuccess() {
    return true;
}

int main() {
    {
        bool success = isSuccess();
        if (success) {}
    }

    // init in if
    if (bool success = isSuccess(); success) {}

    switch (bool success = isSuccess(); success) {
    case true:
        break;
    }

    return 0;
}
