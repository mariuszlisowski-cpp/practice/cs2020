// lambda capture mutable

#include <iostream>

int main() {
    int b = 1;
    
    // must be mutable to write to b
    auto labmda = [=](int a) mutable {
        b = a + 1;
        return b;
    };

    int a = 9;
    std::cout << labmda(a) << std::endl;

    return 0;
}