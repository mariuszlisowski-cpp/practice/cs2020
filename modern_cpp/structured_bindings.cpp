// structured bindings

#include <iostream>
#include <unordered_map>

using Coordinate = std::pair<int, int>;

Coordinate origin() {
    return Coordinate{ 0, 0 };
}

int main() {
    // 1st example
    const auto [x, y] = origin();
    std::cout << "x: " << x << ", y: " << y;

    // 2nd example
    std::unordered_map<std::string, int> mapping{
        { "a", 1 },
        { "b", 2 },
        { "c", 3 }
    };

    // de-structure by reference
    for (const auto& [key, value] : mapping) {}

    return 0;
}
