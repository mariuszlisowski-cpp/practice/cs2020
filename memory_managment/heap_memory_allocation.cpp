// heap memory allocation

// C++ allocation
#include <iostream>

void heapA() {
    int* p = new int(10); // initialize with 10
    delete p;
}

// C allocation equivalent
void heapB() {
    int* p;
    p = (int*)malloc(sizeof(int));
    *p = 100;
    free(p);
}

int main() {

    return 0;
}
