// list sort

#include <iostream>
#include <vector>
#include <list>

std::list<int> createSortedList(const std::vector<int>& v) {
    std::list<int> sorted(v.begin(), v.end());
    sorted.sort();
    return sorted;
}

int main() {
    std::vector<int> vec{2, 3, 4, 1, 6, 5, 8, 7, 9, 0};

    auto list = createSortedList(vec);

    for (const auto& el : list)
        std::cout << el << " ";

    return 0;
}