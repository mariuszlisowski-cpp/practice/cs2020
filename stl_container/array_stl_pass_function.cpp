// array passed to function

#include <array>
#include <iostream>

using namespace std;

template <size_t SIZE>
void display(std::array<int, SIZE> ar) {
    for (const int& e : ar)
        std::cout << e << ' ';
    std::cout << '\n';
}

void displayArray(const auto& arr) {
    for (const auto& e : arr) {
        std::cout << e << ' ';
    }
    std::cout << '\n';
}

template<class T>
void print(T &arr) {
    for (const auto& e : arr) {
        std::cout << e << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::array<int, 5> arr1;
    arr1.fill(1);

    std::array<int, 10> arr2;
    arr2.fill(9);

    display(arr1);
    display(arr2);

    displayArray(arr1);
    displayArray(arr2);

    return 0;
}