// set stl example
// ~ special ASSOCIATIVE container where key is the value
// ~ sorted (values cannot be modified)
// ~ fast search (O(log n))
// ~ slow traversing
// ~ no random access (no [] operator)
// multiset allows duplicated items
// #insert() #find() #erase()

#include <iostream>
#include <set>

using namespace std;

template<class T>
void showSet(set<T>);

int main() {
    set<int> st; // bst tree

    // insert
    st.insert(3);
    st.insert(1);	
    st.insert(4);	
    st.insert(7); // already sorted |1 3 4 7|
    showSet(st);

    // find
    set<int>::iterator it;
    it = st.find(7);
    if (it != st.end())
        cout << *it << " found" << endl;

    // insert fail (not detected) 
    st.insert(3);

    // insert fail detection
    pair<set<int>::iterator, bool> result;
    result = st.insert(3);
    if (!result.second) {
        it = result.first;
        cout << *it << " has been already inserted" << endl;
    }

    // erase by iterator
    st.erase(it); // 9
    // erase by value
    st.erase(7);
    showSet(st);

    // set CANNOT be modified
    //*it = 9;

    return 0;
}

template<class T>
void showSet(set<T> st) {
    for (auto e : st)
        cout << e << " ";
    cout << endl;
}