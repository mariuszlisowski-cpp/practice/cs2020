//  

#include <iostream>
#include <string>
#include <unordered_map>

void foo(const std::unordered_map<char, std::string>& m) {
    // m['S'] = "Sunday"; // ERROR
    // std::cout << m['S']; // ERROR
    auto itr = m.find('S');
    if (itr != m.end())
        std::cout << itr->second;
}

int main() {
    // unique keys accepted
    std::unordered_map<char, std::string> day {
        { 'S', "Sunday"},
        { 'M', "Monday"}
    };

    // inserting new
    day['T'] = "Tuesday";
    day.insert(std::make_pair('W', "Wednesday"));
    std::cout << day.at('T') << std::endl;

    // inserting into exisiting keys
    day.insert(std::make_pair('W', "WEDNESDAY")); // failed
    std::cout << day['W'] << std::endl;
    day['W'] = "WEDNESDAY"; // succeed
    std::cout << day['W'] << std::endl;

    foo(day);

    return 0;
}