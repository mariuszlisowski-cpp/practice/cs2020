// memory reserve in vector

#include <iostream>
#include <vector>

int main() {
    int numberOfElementsToStart = 10;
    
    // reserve by a constructor
    std::vector<int> vecA(numberOfElementsToStart);
    int sizeA = vecA.size();
    int capacityA = vecA.capacity();
    // verbose
    std::cout << "Size: " << sizeA << '\n';
    std::cout << "Capacity: " << capacityA << '\n';
    // size set thus random access
    vecA[5] = 99;

    // reserve by a method
    std::vector<int> vecB;
    vecB.reserve(numberOfElementsToStart); // reserve method
    int sizeB = vecB.size();
    int capacityB = vecB.capacity();
    // verbose
    std::cout << "Size: " << sizeB << '\n';
    std::cout << "Capacity: " << capacityB << '\n';
    // no size thus pushing
    vecB.push_back(99);

    return 0;
}