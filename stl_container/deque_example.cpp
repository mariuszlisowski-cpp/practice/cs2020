// deque example
// double ended queue

#include <iostream>
#include <deque>

using namespace std;

int main() {
    deque<int> deq {2, 3, 4,};

    deq.push_front(1);
    deq.push_back(5);

    for (auto e : deq)
        cout << e << " ";	

    cout << endl << deq[1] << endl;

    return 0;
}