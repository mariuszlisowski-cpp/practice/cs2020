// array stl example
// ~ wrapper for native arrays
// #begin() #end() #size() #swap()

#include <iostream>
#include <array>

using namespace std;

int main() {
    int arr[] {1, 2, 3, 4, 5};

    array<int, 5> a {1, 2, 3, 4, 5};
    cout << "First: " << *a.begin() << endl;
    cout << "Last : " << *(prev(a.end())) << endl;
    cout << "Size : " << a.size() << endl;
    
    array<int, 5> b {10, 20, 30, 40, 50};
    a.swap(b);
    cout << "First: " << *a.begin() << endl;
    cout << "Last : " << *(prev(a.end())) << endl;
    cout << "Size : " << a.size() << endl;

    for (auto e : a)
        cout << e << " ";
    
    return 0;
}