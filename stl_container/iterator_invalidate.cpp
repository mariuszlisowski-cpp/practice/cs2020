// iterator invalidate
// ~ in array based containers

#include <iostream>
#include <vector>


int main() {
    // array based container
    std::vector<int> vec {11, 22, 33, 44};
    int * p = &vec[2]; // points to 33
    auto itr = vec.end() - 1;
    vec.insert(vec.begin(), 10); // {10, 11, 22, 33, 44}

    // invalidated pointers
    std::cout << *itr << '\n'; // undefinied behaviour
    std::cout << *p << '\n'; // undefinied behaviour

    // array based containers:
    // ~ vector
    // ~ deque

    // node base containers: 
    // ~ list
    // ~ associative containers
    // ~ unordered containers

    return 0;
}