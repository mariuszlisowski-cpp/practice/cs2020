// deque instantiate

#include <deque>
#include <forward_list>
#include <iostream>
#include <set>
#include <vector>

int main() {
    std::forward_list<std::string> fl;
    std::deque<std::string> dq;

    // quick copying
    dq = std::deque<std::string>{fl.begin(), fl.end()};

    return 0;
}