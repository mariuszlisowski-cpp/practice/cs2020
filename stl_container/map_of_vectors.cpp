// map of vector

#include <iostream>
#include <map>
#include <vector>

void displayMap(std::map<int, std::vector<int>> map) {
    for (const auto& [key, value] : map) {
        std::cout << "key: " << key << "\n";
        for (const auto& el : value) {
            std::cout << el << ' ';
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}

int main() {
    std::map<int, std::vector<int>> map{
        { 1111, { 1, 2, 3, 4, 5 } },
        { 2222, { 11, 12, 13, 14, 15 } }
    };
    displayMap(map);

    map[1111].resize(10, 9);
    map[2222].resize(10, 1);
    displayMap(map);
}