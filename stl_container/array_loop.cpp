// array loop
// #begin() #end() #range_loop

#include <iostream>

int main() {
    int arr[] {1, 2, 3, 4, 5};

    // arr.begin(); // DOES NOT WORK
    // arr.end();   // DOES NOT WORK

    for (auto it = std::begin(arr); it != std::end(arr); it++)
        std::cout << *it << " ";
    std::cout << std::endl;

    for (auto e : arr)
        std::cout << e << " ";
    std::cout << std::endl;


    return 0;
}
