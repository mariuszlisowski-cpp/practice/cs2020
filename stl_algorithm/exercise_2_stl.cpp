// stl exercise 2
// #generate #copy #equal #mismatch #erase
// #ostream_iterator #reverse_iterator #base()

#include <algorithm>
#include <iostream>
#include <iterator>  // std::ostream_iterator
#include <list>
#include <vector>

// 3. Przekaż odpowiednie iteratory do funkcji std::equal, tak by zwróciła, że oba kontenery są sobie równe.
// 4. Za pomocą std::mismatch oraz erase, usuń niepasujące elementy z listy
// 5. Zawołaj funkcję std::equal dla pełnych zakresów aby upewnić się, że są teraz identyczne.

template <class T>
void displayContainer(T& cont) {
    std::copy(cont.begin(), cont.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    // fill list [1, 10]
    std::list<int> l(10);
    std::generate(l.begin(), l.end(),
                  [i = 0]() mutable {
                      return ++i;
                  });
    displayContainer(l);

    // fill vector [5, 10]
    std::vector<int> v(6);
    int j{ 5 };
    std::generate(v.begin(), v.end(),
                  [&j]() {
                      return j++;
                  });
    displayContainer(v);

    // index from which list & vector are equal
    if (std::equal(std::next(l.begin(), 4), l.end(),
                   v.begin())) {
        std::cout << "Equal form index 4\n";
    }

    // copy list to a NEW one
    std::list<int> ll(l.size());
    std::copy(l.begin(), l.end(),
              ll.begin());

    // remove mismatched elements from a vector
    auto itr_pair = std::mismatch(l.rbegin(), l.rend(),
                                  v.rbegin());
    l.erase(l.begin(),                          // regular iterator
            std::next(itr_pair.first).base());  // reverse iterator
    displayContainer(l);

    // remove matched elements from a NEW list
    itr_pair = std::mismatch(ll.rbegin(), ll.rend(),
                             v.rbegin());
    l.erase(std::next(itr_pair.first).base(),  // reverse iterator
            std::next(ll.rend()).base());      // reverse iterator
    displayContainer(ll);

    return 0;
}
