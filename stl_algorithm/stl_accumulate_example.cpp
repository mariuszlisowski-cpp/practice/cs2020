// accumulate example 

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

std::vector<int> fillVector(size_t size) {
    std::vector<int> vec(size);
    std::iota(vec.begin(),
              vec.end(),
              1); // initial value then incremented (++)
    return vec;
}

int multiply(const std::vector<int>& vec) {
    return std::accumulate(vec.begin(),
                           vec.end(),
                           1, // initial value of multiplying (cannot be zero)
                           std::multiplies<int>());
}

int main() {
    std::cout << multiply(fillVector(9)) << std::endl;

    return 0;
}
