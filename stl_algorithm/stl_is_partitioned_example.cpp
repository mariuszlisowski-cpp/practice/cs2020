// stl is_partitioned example

#include <algorithm>
#include <array>
#include <iostream>
 

template <class T>
void display(T& arr) {
    for (const auto& el : arr) {
        std::cout << el << ' ';
    }
    std::cout << ' ';
}

int main()
{
    std::array<int, 9> v { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    // display(v);

    // true if even element
    auto is_even = [](int i){ return i % 2 == 0; };
        
    std::cout.setf(std::ios_base::boolalpha);
    display(v);
    std::cout << std::is_partitioned(v.begin(), v.end(), is_even) << '\n';
 
    std::partition(v.begin(), v.end(), is_even);
    display(v);
    std::cout << std::is_partitioned(v.begin(), v.end(), is_even) << '\n';
 
    std::reverse(v.begin(), v.end());
    display(v);
    std::cout << std::is_partitioned(v.begin(), v.end(), is_even);

    return 0;
}