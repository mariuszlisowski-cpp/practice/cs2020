// ways of reversing vector

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

void printVec(std::vector<int>& vec) {
    for (auto i : vec) {
        std::cout << i << " ";
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> vector{ 1, 0, 5, 2, 7 };
    std::vector<int> reversed;
    std::cout << "Original vector: " << '\n';
    printVec(vector);

    std::cout << "Start reversing..." << '\n';

    for (auto it = vector.crbegin(); it != vector.crend(); ++it) {
        reversed.push_back(*it);
    }
    std::copy(vector.crbegin(), vector.crend(),
              reversed.begin());
    
    std::copy(vector.cbegin(), vector.cend(),
              reversed.rbegin());
    
    std::copy_backward(vector.crbegin(), vector.crend(),
                       reversed.end());
    
    std::copy_backward(vector.cbegin(), vector.cend(),
                       reversed.rend());
    
    std::copy_if(vector.crbegin(), vector.crend(),
                 reversed.begin(),
                 [](int i) { return true; });
    
    std::copy_if(vector.cbegin(), vector.cend(),
                 reversed.rbegin(),
                 [](int i) { return true; });
    
    std::copy_n(vector.crbegin(), vector.size(),
                reversed.begin());
    
    std::copy_n(vector.cbegin(), vector.size(),
                reversed.rbegin());
    
    std::move(vector.crbegin(), vector.crend(),
              reversed.begin());
    
    std::move(vector.cbegin(), vector.cend(),
              reversed.rbegin());
    
    std::move_backward(vector.crbegin(), vector.crend(),
                       reversed.end());
    
    std::move_backward(vector.cbegin(), vector.cend(),
                       reversed.rend());
    
    std::reverse(vector.begin(), vector.end());

    // reverse original vector
    std::reverse(vector.rbegin(), vector.rend());
    // back to original vector

    std::reverse_copy(vector.cbegin(), vector.cend(),
                      reversed.begin());
    
    std::reverse_copy(vector.crbegin(), vector.crend(),
                      reversed.rbegin());

    std::transform(vector.crbegin(), vector.crend(),
                   reversed.begin(),
                   [](int i) { return i; });
    
    std::transform(vector.cbegin(), vector.cend(),
                   reversed.rbegin(),
                   [](int i) { return i; });

    return 0;
}
