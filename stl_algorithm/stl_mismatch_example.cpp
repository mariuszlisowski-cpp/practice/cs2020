// stl mismatch example

#include <algorithm>
#include <iostream>
#include <vector>

std::string mirror_ends(const std::string& in) {
    // [first, last)
    return std::string(in.begin(),
                       std::mismatch(in.begin(), in.end(), in.rbegin()).first);
}

int main() {
    std::cout << mirror_ends("abXYZba") << '\n'
              << mirror_ends("abca") << '\n'
              << mirror_ends("aba") << '\n';

    return 0;
}
