// stable partition example 

#include <algorithm>
#include <iostream>
#include <vector>

void Gather(std::vector<char>& vec) {
    auto pred {[&](auto& el){
        return el != '*'; // true if not '*'
    }};

    auto middle = vec.begin() + vec.size() / 2;
    std::stable_partition(vec.begin(),
                          middle, pred); // if 'pred' false '*' goes to end (here: middle)
    std::stable_partition(middle,
                          vec.end(),
                          [&](auto& el) {
                              return !pred(el); // not not '*' (i.e. '*')
                          }); // if 'pred' true '*' goes to begining (here: middle)
}

void PrintVec(const std::vector<char>& vec) {
    for (const auto el : vec) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<char> vec {'*', '$', '@', '*', '#', '@', '^', '*', '(', ')', '$', '*'};
    PrintVec(vec);
    Gather(vec);
    PrintVec(vec);

    return 0;
}
