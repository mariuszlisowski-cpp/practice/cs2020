// enum class example

enum class Languages {
    ENGLISH,
    GERMAN,
    POLISH
};

int main() {
    Languages a = Languages::ENGLISH;
    
    // Languages b = GERMAN;       // error
    // int c = Languages::ENGLISH; // error

    int d = static_cast<int>(Languages::ENGLISH); // only explicit cast allowed
}
