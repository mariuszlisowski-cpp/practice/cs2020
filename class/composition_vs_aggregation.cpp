// composition vs aggregation

// composition
// MUST be a part of another object
class CPU {}; // part of a coputer

class Computer {
public:
    CPU cpu_; // underscore
    RAM* ram_;
};

// aggregation
// MAY be a part of another object
class RAM {}; // can exist separately (be added)

int main(){
    // composition example
    Computer computer; // CPU inside (comp 1 : 1 cpu)

    // aggregation example
    RAM ram;
    computer.ram_ = &ram; // ram (comp 0..1 : 0..n ram)

    return 0;
} 
